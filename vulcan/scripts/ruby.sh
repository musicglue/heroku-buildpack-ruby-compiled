#!/bin/bash

set -e
set -o pipefail

mkdir -p /app/local /tmp/build
pushd /tmp/build

echo "+ Fetching Ruby sources..."
curl -sSL http://cache.ruby-lang.org/pub/ruby/2.0/ruby-2.0.0-p247.tar.gz -o - | tar xz
pushd ruby-2.0.0-p247

echo "+ Configuring Ruby..."
./configure --prefix=/app/local --disable-install-doc

echo "+ Compiling Ruby..."
make && make install
popd

echo "+ Fetching Rubygems sources..."
curl -sSL http://production.cf.rubygems.org/rubygems/rubygems-2.1.7.tgz -o - | tar xz
pushd rubygems-2.1.7

export GEM_HOME="/app/local/lib/ruby/gems/2.0.0"
export PATH="$GEM_HOME/bin:/app/local/bin:$PATH"
export LANG=en_GB.utf-8

echo "+ Installing Rubygems..."
ruby setup.rb

echo "+ Installing Bundler..."
gem install bundler

popd
echo "+ Done!"
